//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.md for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
#include <lcl/FieldAccessor.h>

#include <array>
#include <cassert>
#include <iostream>
#include <vector>

namespace
{

const int readValues[] = { 9, 15, 16, 3 };
const int writeValues[] = { 12, 6, 18, 14 };

template <typename VecType>
void VecAccessorReadTest(const VecType& vec, int numComps)
{
  for (int i = 0; i < numComps; ++i)
  {
    auto exp = static_cast<lcl::ComponentType<VecType>>(readValues[i]);
    auto got = lcl::component(vec, i);
    if (got != exp)
    {
      std::cout << "Incorrect read value. Expected = " << exp << ", Got = " << got << "\n";
    }
  }
}

template <typename VecType>
void VecAccessorWriteTest(VecType&& vec, int numComps)
{
  for (int i = 0; i < numComps; ++i)
  {
    lcl::component(std::forward<VecType>(vec), i) =
      static_cast<lcl::ComponentType<VecType>>(writeValues[i]);
  }
}

template <typename T>
struct CustomVec
{
  T x, y, z, w;
};

template <typename T>
inline T& ValueAt(CustomVec<T>& vec, int idx)
{
  switch(idx)
  {
    default: assert(false);
    case 0: return vec.x;
    case 1: return vec.y;
    case 2: return vec.z;
    case 3: return vec.w;
  }
}

template <typename T>
inline const T& ValueAt(const CustomVec<T>& vec, int idx)
{
  switch(idx)
  {
    default: assert(false);
    case 0: return vec.x;
    case 1: return vec.y;
    case 2: return vec.z;
    case 3: return vec.w;
  }
}

template <typename VecType>
void InitRead(VecType& vec, int numComps)
{
  for (int i = 0; i < numComps; ++i)
  {
    lcl::component(vec, i) = static_cast<lcl::ComponentType<VecType>>(readValues[i]);
  }
}

template <typename T>
void InitRead(CustomVec<T>& vec, int numComps)
{
  for (int i = 0; i < numComps; ++i)
  {
    ValueAt(vec, i) = static_cast<T>(readValues[i]);
  }
}

template <typename VecType>
void VerifyWrite(const VecType& vec, int numComps)
{
  for (int i = 0; i < numComps; ++i)
  {
    auto exp = static_cast<lcl::ComponentType<VecType>>(writeValues[i]);
    auto got = lcl::component(vec , i);
    if (got != exp)
    {
      std::cout << "Incorrect value written. Expected = " << exp << ", Got = " << got << "\n";
    }
  }
}

template <typename T>
void VerifyWrite(const CustomVec<T>& vec, int numComps)
{
  for (int i = 0; i < numComps; ++i)
  {
    auto exp = static_cast<T>(writeValues[i]);
    auto got = ValueAt(vec, i);
    if (got != exp)
    {
      std::cout << "Incorrect value written. Expected = " << exp << ", Got = " << got << "\n";
    }
  }
}

template <typename T, int NumComponents>
void TestVecAccessorsOfTypeAndSize()
{
  //---------------------------------------------------------------------------
  // Test C style arrays
  std::cout << "\t\tTest reading C style arrays\n";
  T cArray[NumComponents];
  InitRead(cArray, NumComponents);
  VecAccessorReadTest(cArray, NumComponents);

  std::cout << "\t\tTest writing C style arrays\n";
  VecAccessorWriteTest(cArray, NumComponents);
  VerifyWrite(cArray, NumComponents);

  //---------------------------------------------------------------------------
  // Test C++ style arrays
  std::cout << "\t\tTest reading C++ style arrays\n";
  std::array<T, NumComponents> cxxArray;
  InitRead(cxxArray, NumComponents);
  VecAccessorReadTest(cxxArray, NumComponents);

  std::cout << "\t\tTest writing C++ style arrays\n";
  VecAccessorWriteTest(cxxArray, NumComponents);
  VerifyWrite(cxxArray, NumComponents);

  //---------------------------------------------------------------------------
  // Test C++ vectors
  std::cout << "\t\tTest reading std::vector\n";
  std::vector<T> cxxVector(NumComponents);
  InitRead(cxxVector, NumComponents);
  VecAccessorReadTest(cxxVector, NumComponents);

  std::cout << "\t\tTest writing std::vector\n";
  VecAccessorWriteTest(cxxVector, NumComponents);
  VerifyWrite(cxxVector, NumComponents);

  //---------------------------------------------------------------------------
  // Test custom Vec type as functor
  std::cout << "\t\tTest reading custom vec type as functor\n";
  CustomVec<T> customVec;
  InitRead(customVec, NumComponents);
  VecAccessorReadTest([&customVec](int idx) -> const T& { return ValueAt(customVec, idx); }, NumComponents);

  std::cout << "\t\tTest writing custom vec type as functor\n";
  VecAccessorWriteTest([&customVec](int idx) -> T& { return ValueAt(customVec, idx); }, NumComponents);
  VerifyWrite(customVec, NumComponents);
}

template <typename T>
void TestVecAccessorOnScalar()
{
  std::cout << "\t\tTest reading scalar\n";
  T scalar = static_cast<T>(readValues[0]);
  VecAccessorReadTest(scalar, 1);

  std::cout << "\t\tTest writing scalar\n";
  VecAccessorWriteTest(scalar, 1);
  if (scalar != static_cast<T>(writeValues[0]))
  {
    std::cout << "Incorrect value written. Expected = " << static_cast<T>(writeValues[0])
              << ", Got = " << scalar << "\n";
  }
}

template <typename T>
void TestVecAccessorsOfType()
{
  std::cout << "\tTesting scalar\n";
  TestVecAccessorOnScalar<T>();

  std::cout << "\tTesting size 1\n";
  TestVecAccessorsOfTypeAndSize<T, 1>();

  std::cout << "\tTesting size 2\n";
  TestVecAccessorsOfTypeAndSize<T, 2>();

  std::cout << "\tTesting size 3\n";
  TestVecAccessorsOfTypeAndSize<T, 3>();

  std::cout << "\tTesting size 4\n";
  TestVecAccessorsOfTypeAndSize<T, 4>();
}

void UnitTestVecAccessors()
{
  std::cout << "Testing type int\n";
  TestVecAccessorsOfType<int>();
  std::cout << "Testing type float\n";
  TestVecAccessorsOfType<float>();
}

} // anonymous namespace

int main(int, char*[])
{
  UnitTestVecAccessors();
}
