##============================================================================
##  Copyright (c) Kitware, Inc.
##  All rights reserved.
##  See LICENSE.md for details.
##
##  This software is distributed WITHOUT ANY WARRANTY; without even
##  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE.  See the above copyright notice for more information.
##============================================================================
import math
import random
import sys
import time
import vtk

# initialize
seed = int(time.time())
sys.stderr.write("using seed = " + str(seed) + "\n")
random.seed(seed)

#==============================================================================
def vecAdd(v1, v2):
  return [(a + b) for a, b in zip(v1, v2)]

def vecSubtract(v1, v2):
  return [(a - b) for a, b in zip(v1, v2)]

def vecScalarMultiply(v, s):
  return [x * s for x in v]

def vecCross(v1, v2):
  return [v1[1] * v2[2] - v1[2] * v2[1],
          v1[2] * v2[0] - v1[0] * v2[2],
          v1[0] * v2[1] - v1[1] * v2[0]]

def vecDot(v1, v2):
  return sum([(a * b) for a, b in zip(v1, v2)])

def vecMag(v):
  return math.sqrt(vecDot(v, v))

def vecNormal(v):
  mag = vecMag(v)
  return [(x / mag) for x in v]

def vec2str(v):
  str = "["
  if v:
    for c in v: str += "{0:0.3f} ".format(c)
  str += "]"
  return str

#==============================================================================
class Plane:
  def __init__(self, pt1, pt2, pt3):
    self.points = [pt1, pt2, pt3]

  def origin(self):
    return self.points[0]

  def normal(self):
    v1 = vecSubtract(self.points[1], self.points[0])
    v2 = vecSubtract(self.points[2], self.points[0])
    return vecNormal(vecCross(v1, v2))

  def evaluate(self, pt):
    return vecDot(self.normal(), vecSubtract(pt, self.points[0]))

  def closestPoint(self, pt):
    v1 = vecSubtract(pt, self.points[0])
    n = self.normal()
    dist = vecDot(n, v1)
    return vecSubtract(pt, vecScalarMultiply(n, dist))


#------------------------------------------------------------------------------
def ThreePlanesIntersectionPoint(plane1, plane2, plane3):
  n1 = plane1.normal()
  n2 = plane2.normal()
  n3 = plane3.normal()

  denom = vecDot(n1, vecCross(n2, n3))
  if abs(denom) < 0.01:
    return None

  d1 = vecDot(n1, plane1.points[0])
  d2 = vecDot(n2, plane2.points[0])
  d3 = vecDot(n3, plane3.points[0])

  q1 = vecScalarMultiply(vecCross(n2, n3), d1)
  q2 = vecScalarMultiply(vecCross(n3, n1), d2)
  q3 = vecScalarMultiply(vecCross(n1, n2), d3)

  return vecScalarMultiply(vecAdd(q1, vecAdd(q2, q3)), 1.0/denom)

#==============================================================================
# Checks if the given set of co-planar points form a convex-polygon
# Assuming CCW winding, the first three points determine the plane's normal
def IsConvex(points):
  npts = len(points)
  if npts <= 3:
    return True

  normal = None
  for i in range(0, npts):
    pt1 = points[i]
    pt0 = points[(i + 1) % npts]
    pt2 = points[(i + 2) % npts]
    v1 = vecNormal(vecSubtract(pt1, pt0))
    v2 = vecNormal(vecSubtract(pt2, pt0))
    cp = vecCross(v2, v1)
    if not normal: normal = vecNormal(cp)
    elif vecDot(cp, normal) < 0.1: return False
  return True

def ApplyRandomTransformation(points):
  centroid = [ 0.0, 0.0, 0.0 ]
  for x in points:
    for i in [0, 1, 2]:
      centroid[i] += x[i]

  for i in [0, 1, 2]:
    centroid[i] /= len(points)

  transform = vtk.vtkTransform()
  transform.PostMultiply()
  transform.Identity()
  transform.Translate(-centroid[0], -centroid[1], -centroid[2])
  transform.RotateX(random.uniform(-90, 90))
  transform.RotateY(random.uniform(0, 360))
  transform.Translate([random.uniform(-8, 8) for i in [0, 1, 2]])

  points[:] = [transform.TransformPoint(x) for x in points]
  return points

def WriteCell(cellType, points, fileName):
  pa = vtk.vtkDoubleArray()
  pa.SetNumberOfComponents(3)
  for pt in points:
    pa.InsertNextTypedTuple(pt)
  pts = vtk.vtkPoints()
  pts.SetData(pa)

  ca = vtk.vtkCellArray()
  ca.InsertNextCell(len(points), range(0, len(points)))

  ug = vtk.vtkUnstructuredGrid()
  ug.SetPoints(pts)
  ug.SetCells(cellType, ca)

  writer = vtk.vtkUnstructuredGridWriter()
  writer.SetFileName(fileName)
  writer.SetInputData(ug)
  writer.Update()

def BuildCell(cell, pts):
  points = vtk.vtkPoints()
  points.SetDataTypeToFloat()
  points.Resize(len(pts))
  for pt in pts:
    points.InsertNextPoint(pt)
  cell.Initialize(len(pts), points)

def GenerateRandomPoint(bounds, axisMask):
  pt = [0.0, 0.0, 0.0]
  for i in [0, 1, 2]:
    if axisMask[i]: pt[i] = random.uniform(bounds[0][i], bounds[1][i])
  return pt

#------------------------------------------------------------------------------
bds = [[0.0, 0.0, 0.0], [1.0, 1.0, 1.0]]

def GenerateRandomVertex():
  points = [GenerateRandomPoint(bds, [1, 1, 1])]
  cell = vtk.vtkVertex()
  BuildCell(cell, points)
  return cell

def GenerateRandomLine():
  points = [GenerateRandomPoint(bds, [1, 1, 1]) for i in (0, 1)]
  cell = vtk.vtkLine()
  BuildCell(cell, points)
  return cell

def GenerateRandomTriangle():
  points = []
  passed = False
  while not passed:
    points = [GenerateRandomPoint(bds, [1, 1, 1]) for i in (0, 1, 2)]
    passed = vecDot(vecSubtract(points[1], points[0]), vecSubtract(points[2], points[0])) != 0.0

  cell = vtk.vtkTriangle()
  BuildCell(cell, points)
  return cell

def ConvexHull(points):
  # find left-most point
  firstPoint = points[0]
  for pt in points:
    if pt[0] < firstPoint[0]:
      firstPoint = pt

  result = [firstPoint]
  curEdgeVec = [0, 1, 0]
  closed = False
  while not closed:
    nextPoint = []
    nextEdgeVec = []
    maxAngle = -2
    for pt in points:
      edgeVec = vecSubtract(pt, result[-1])
      if vecDot(edgeVec, edgeVec) < 0.001: continue
      edgeVec = vecNormal(edgeVec)
      angle = -vecDot(curEdgeVec, edgeVec)
      if angle > maxAngle:
        maxAngle = angle
        nextPoint = pt
        nextEdgeVec = vecScalarMultiply(edgeVec, -1)
    if nextPoint == firstPoint:
      closed = True
    else:
      points.remove(nextPoint)
      result.append(nextPoint)
      curEdgeVec = nextEdgeVec
  return result

def GenerateRandomPolygon():
  points = []
  passed = False
  while not passed:
    points = ConvexHull([GenerateRandomPoint(bds, [1, 1, 0]) for i in range(0, 12)])
    passed = IsConvex(points)

  ApplyRandomTransformation(points)

  cell = vtk.vtkPolygon()
  BuildCell(cell, points)
  return cell

def GenerateRandomQuad():
  points = []
  passed = False
  while not passed:
    points = [GenerateRandomPoint(bds, [1, 1, 0]) for i in range(0, 4)]
    passed = IsConvex(points)

  ApplyRandomTransformation(points)

  cell = vtk.vtkQuad()
  BuildCell(cell, points)
  return cell

def GenerateRandomPixel():
  x1 = random.randint(0, 2)
  x2 = x1 + 1
  if x2 == 3: x1, x2 = 0, 2

  origin = GenerateRandomPoint([[-8, -8, -8], [8, 8, 8]], [1, 1, 1])
  spacing = GenerateRandomPoint([[0.1, 0.1, 0.1], [1, 1, 1]], [1, 1, 1])

  points = [[origin[i] for i in [0, 1, 2]] for j in range(0, 4)]
  points[1][x1] += spacing[x1]
  points[2][x2] += spacing[x2]
  points[3][x1] += spacing[x1]
  points[3][x2] += spacing[x2]

  cell = vtk.vtkPixel()
  BuildCell(cell, points)
  return cell

def GenerateRandomTetra():
  points = []
  passed = False
  while not passed:
    points = [GenerateRandomPoint(bds, [1, 1, 1]) for i in range(0, 4)]
    v1 = vecSubtract(points[1], points[0])
    v2 = vecSubtract(points[2], points[0])
    v3 = vecSubtract(points[3], points[0])
    passed = vecDot(vecCross(v1, v2), v3) > 0.0

  cell = vtk.vtkTetra()
  BuildCell(cell, points)
  return cell

def GenerateRandomHex():
  faces = [[0, 4, 7, 3], [1, 2, 6, 5], [0, 1, 5, 4], [3, 7, 6, 2], [0, 3, 2, 1], [4, 5, 6, 7]]

  # first generate face 4 (quad)
  f4 = []
  passed = False
  while not passed:
    f4 = [GenerateRandomPoint(bds, [1, 1, 0]) for i in range(0, 4)]
    passed = IsConvex(f4)

  # correct orientation
  p4 = Plane(f4[0], f4[1], f4[2])
  if p4.normal()[2] > 0.0:
    f4.reverse()

  points = [f4[0], f4[3], f4[2], f4[1], [], [], [], []]

  passed = False
  while not passed:
    points[4] = GenerateRandomPoint(bds, [1, 1, 1])
    points[5] = GenerateRandomPoint(bds, [1, 1, 1])
    points[7] = GenerateRandomPoint(bds, [1, 1, 1])

    # face 0
    p0 = Plane(points[0], points[4], points[3])
    points[7] = p0.closestPoint(points[7])
    f0 = [points[i] for i in faces[0]]
    if not IsConvex(f0): continue

    # face 2
    p2 = Plane(points[0], points[1], points[4])
    points[5] = p2.closestPoint(points[5])
    f2 = [points[i] for i in faces[2]]
    if not IsConvex(f2): continue

    # find point 6
    p1 = Plane(points[1], points[2], points[5])
    p3 = Plane(points[3], points[7], points[2])
    p5 = Plane(points[4], points[5], points[7])
    points[6] = ThreePlanesIntersectionPoint(p1, p3, p5)
    if not points[6]: continue

    passed = True
    for fn in [[points[i] for i in faces[n]] for n in [1, 3, 5]]:
      passed &= IsConvex(fn)

  ApplyRandomTransformation(points)

  cell = vtk.vtkHexahedron()
  BuildCell(cell, points)
  return cell

def GenerateRandomVoxel():
  origin = GenerateRandomPoint([[-8, -8, -8], [8, 8, 8]], [1, 1, 1])
  spacing = GenerateRandomPoint([[0.1, 0.1, 0.1], [1, 1, 1]], [1, 1, 1])

  verts = [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0], [0, 0, 1], [1, 0, 1], [0, 1, 1], [1, 1, 1]]
  points = [[(origin[i] + (v[i] * spacing[i])) for i in [0, 1, 2]] for v in verts]

  cell = vtk.vtkVoxel()
  BuildCell(cell, points)
  return cell

def GenerateRandomWedge():
  faces = [[0, 1, 2], [3, 5, 4], [0, 3, 4, 1], [1, 4, 5, 2], [2, 5, 3, 0]]
  f2 = []
  passed = False
  while not passed:
    f2 = [GenerateRandomPoint(bds, [1, 1, 0]) for i in range(0, 4)]
    passed = IsConvex(f2)

  pl = Plane(f2[0], f2[1], f2[2])
  if pl.normal()[2] > 0.0:
    f2.reverse()

  points = [f2[0], f2[3], [], f2[1], f2[2], []]

  passed = False
  while not passed:
    points[2] = GenerateRandomPoint([[0.0, 0.0, 0.01], bds[1]], [1, 1, 1])

    p3 = Plane(points[1], points[4], points[2])
    p4 = Plane(points[0], points[2], points[3])
    v = vecNormal(vecCross(p4.normal(), p3.normal()))
    points[5] = vecAdd(points[2], vecScalarMultiply(v, random.uniform(0.01, 1.0)))

    f3 = [points[i] for i in faces[3]]
    f4 = [points[i] for i in faces[4]]
    passed = IsConvex(f3) and IsConvex(f4)

  ApplyRandomTransformation(points)

  cell = vtk.vtkWedge()
  BuildCell(cell, points)
  return cell

def GenerateRandomPyramid():
  points = []
  passed = False
  while not passed:
    points = [GenerateRandomPoint(bds, [1, 1, 0]) for i in range(0, 4)]
    passed = IsConvex(points)

  pl = Plane(points[0], points[1], points[2])
  if pl.normal()[2] < 0.0:
    points.reverse()
  points.append(GenerateRandomPoint([[0.0, 0.0, 0.01], bds[1]], [1, 1, 1]))

  ApplyRandomTransformation(points)

  cell = vtk.vtkPyramid()
  BuildCell(cell, points)
  return cell

#==============================================================================
numInsideTestPoints = 4
numOutsideTestPoints = 4

# The polygon cell of VTK and LCL (based on VTK-m original implementation) are
# different.
def GenerateTestParasPolygon(points, bounds):
  numPoints = len(points)
  cellDim = 2

  fieldNumComponents = random.randint(1, 3)
  gradients = [[random.uniform(-5, 5) for i in (0, 1, 2)] for j in range(0, fieldNumComponents)]
  originValue = [random.uniform(-8, 8) for i in range(0, fieldNumComponents)]
  fieldValue = lambda pt : [o + sum([pt[i] * d[i] for i in (0, 1, 2)]) for o, d in zip(originValue, gradients)]

  fa = vtk.vtkDoubleArray()
  fa.SetNumberOfComponents(fieldNumComponents)
  fa.SetNumberOfTuples(numPoints)

  field = []
  for idx in range(0, numPoints):
    fval = fieldValue(points[idx])
    if len(fval) == 1:
      field.append(fval[0])
    else:
      field.append(fval)
    fa.SetTypedTuple(idx, fval)

  polygonPlane = Plane(points[0], points[numPoints/3], points[(2*numPoints)/3])
  polygonNormal = polygonPlane.normal()

  insides = []
  outsides = []
  while len(insides) < numInsideTestPoints or len(outsides) < numOutsideTestPoints:
    wc = [random.uniform(bounds[2*i], bounds[2*i + 1]) for i in [0, 1, 2]]

    # is it inside or outside
    isInside = True
    for i in range(0, numPoints):
      p0 = points[i]
      p1 = points[(i + 1) % numPoints]
      edge = vecSubtract(p1, p0)
      v1 = vecCross(polygonNormal, edge)
      v2 = vecSubtract(wc, p0)
      if vecDot(v1, v2) < 0:
        isInside = False
        break

    if not isInside and len(outsides) < numOutsideTestPoints:
      outsides.append([wc, [0, 0], 0])
    elif isInside and len(insides) < numInsideTestPoints:
      interp = fieldValue(polygonPlane.closestPoint(wc))
      if len(interp) == 1: interp = interp[0]

      # compute derivative on a triangle in polygon's plane
      tri = vtk.vtkTriangle()
      triPts = [points[0], points[numPoints/3], points[2*numPoints/3]]
      triField = []
      for pt in triPts:
        triField.extend(fieldValue(pt))
      BuildCell(tri, triPts)
      vtkDeriv = [0]*fieldNumComponents*3
      tri.Derivatives(0, [0, 0, 0], triField, fieldNumComponents, vtkDeriv)

      deriv = vtkDeriv
      if fieldNumComponents != 1:
        deriv = [[vtkDeriv[i] for i in range(j, len(vtkDeriv), 3)] for j in [0, 1, 2]]

      insides.append([wc, [0, 0], interp, deriv])

  return [field, insides, outsides]

def GenerateTestParasGeneral(cell, points, bounds):
  numPoints = cell.GetNumberOfPoints()
  cellDim = cell.GetCellDimension()

  fieldNumComponents = random.randint(1, 3)
  fa = vtk.vtkDoubleArray()
  fa.SetNumberOfComponents(fieldNumComponents)
  fa.SetNumberOfTuples(numPoints)

  field = []
  for idx in range(0, numPoints):
    fval = [random.uniform(-5, 5) for i in range(0, fieldNumComponents)]
    if fieldNumComponents == 1:
      field.append(fval[0])
    else:
      field.append(fval)
    fa.SetTypedTuple(idx, fval)

  insides = []
  outsides = []
  while len(insides) < numInsideTestPoints or len(outsides) < numOutsideTestPoints:
    wc = [0, 0, 0]
    if len(insides) < numInsideTestPoints and cellDim == 0: # special case for vertex
      wc = points[0]
    elif len(insides) < numInsideTestPoints and cellDim == 1: # special case for line
      lineVec = vecSubtract(points[1], points[0])
      t = random.uniform(0, 1)
      wc = vecAdd(points[0], vecScalarMultiply(lineVec, t))
    else:
      wc = [random.uniform(bounds[2*i], bounds[2*i + 1]) for i in [0, 1, 2]]

    cp = [0, 0, 0]
    pc = [0, 0, 0]
    dist = vtk.reference(0)
    weights = [0]*numPoints
    status = cell.EvaluatePosition(wc, cp, vtk.reference(0), pc, dist, weights)

    if status == 0 and len(outsides) < numOutsideTestPoints:
      # parametric distance
      pdist = cell.GetParametricDistance(pc)

      outsides.append([wc, pc[0:cellDim], pdist])
    elif status == 1 and len(insides) < numInsideTestPoints:
      # interpolation
      a = vtk.vtkDoubleArray()
      a.SetNumberOfComponents(fieldNumComponents)
      a.SetNumberOfTuples(1)
      a.InterpolateTuple(0, cell.GetPointIds(), fa, weights)
      if fieldNumComponents == 1:
        interp = a.GetValue(0)
      else:
        interp = [a.GetValue(i) for i in range(0, a.GetNumberOfValues())]

      #derivative
      result = [0]*(fieldNumComponents * 3)
      flatField = [fa.GetValue(i) for i in range(0, fa.GetNumberOfValues())]
      cell.Derivatives(0, pc, flatField, fieldNumComponents, result)
      if fieldNumComponents == 1:
        deriv = result
      else:
        deriv = [[result[i] for i in range(j, len(result), 3)] for j in [0, 1, 2]]

      insides.append([wc, pc[0:cellDim], interp, deriv])

  return [field, insides, outsides]

def GenerateTestParas(cell):
  points = [cell.GetPoints().GetPoint(i) for i in range(0, cell.GetNumberOfPoints())]

  bounds = [0]*6
  cell.GetBounds(bounds)
  # pad out the bounds a bit
  for i in (0, 1, 2):
    length = bounds[2*i + 1] - bounds[2*i]
    pad = max(0.1, length * 0.1)
    bounds[2*i] -= pad
    bounds[2*i + 1] += pad

  field = []
  insides = []
  outsides = []
  if cell.GetCellType() == 7: # Polygon
    result = GenerateTestParasPolygon(points, bounds)
    field = result[0]
    insides = result[1]
    outsides = result[2]
  else: # everything else
    result = GenerateTestParasGeneral(cell, points, bounds)
    field = result[0]
    insides = result[1]
    outsides = result[2]

  fieldNumComponents = 1
  if type(field[0]) is list: fieldNumComponents = len(field[0])

  wcoords = []
  pcoords = []
  pdist = []
  interp = []
  derivs = []

  for p in insides:
    wcoords.append(p[0])
    pcoords.append(p[1])
    interp.append(p[2])
    derivs.append(p[3])
  for p in outsides:
    wcoords.append(p[0])
    pcoords.append(p[1])
    pdist.append(p[2])

  if cell.GetCellType() == 8: # Pixel
    points[2], points[3] = points[3], points[2]
    field[2], field[3] = field[3], field[2]
  elif cell.GetCellType() == 11: # Voxel
    points[2], points[3] = points[3], points[2]
    points[6], points[7] = points[7], points[6]
    field[2], field[3] = field[3], field[2]
    field[6], field[7] = field[7], field[6]

  return [cell.GetCellType(), points, fieldNumComponents, field, wcoords, pcoords, pdist, interp, derivs]

#==============================================================================
def GetFormattedString(value, padding, precision):
  if type(value) is list or type(value) is tuple:
    substrs = [GetFormattedString(x, padding, precision) for x in value]
    if not substrs:
      return ""
    output = substrs[0]
    for ss in substrs[1:]:
      if ss: output += " " + ss
    return output
  else:
    return ("{:" + str(padding) + "." + str(precision) + "f}").format(value)

def PrintArrayRec(array, padding, precision, depth):
  text = ""
  if depth >= 3:
    for subArray in array:
      text += PrintArrayRec(subArray, padding, precision, depth - 1)
  else:
    fstr = GetFormattedString(array[0], padding, precision)
    col = len(fstr)
    text += fstr
    for val in array[1:]:
      fstr = GetFormattedString(val, padding, precision)
      if len(fstr) == 0: continue
      if (col + 4 + len(fstr)) >= 80:
        text += "\n"
        col = 0
      else:
        delim = " " * (4**(depth - 1))
        text += delim
        col += len(delim)

      text += fstr
      col += len(fstr)
    text += "\n"
  return text

def MaxAbsValueAndDepthRec(var):
  if type(var) is list or type(var) is tuple:
    if len(var) == 0:
      return [0, 0]
    rec = max([MaxAbsValueAndDepthRec(x) for x in var])
    return [rec[0], rec[1] + 1]
  else:
    return [abs(var), 0]

def GetMaxNumDigitsAndDepth(array):
  maxAbsValueAndDepth = MaxAbsValueAndDepthRec(array)
  numDigits = int(math.log10(maxAbsValueAndDepth[0] + 10))
  return [numDigits, maxAbsValueAndDepth[1]]

def PrintArray(array, precision):
  numDigitsAndDepth = GetMaxNumDigitsAndDepth(array)
  padding = numDigitsAndDepth[0] + precision + 3
  depth = numDigitsAndDepth[1]
  return PrintArrayRec(array, padding, precision, depth)

def PrintTestParas(paras):
  text = ""
  text += "# Cell Shape, Number of points, Field number of components\n"
  text += str(paras[0]) + " " + str(len(paras[1])) + " " + str(paras[2]) + "\n"

  text += "# Points\n"
  text += PrintArray(paras[1], 10)

  text += "# Field\n"
  text += PrintArray(paras[3], 10)

  text += "# Test points world coordinates\n"
  text += PrintArray(paras[4], 10)

  text += "# Test points parametric coordinates\n"
  text += PrintArray(paras[5], 10)

  text += "# Parametric distance (outside points only)\n"
  text += PrintArray(paras[6], 4)

  text += "# Field interpolation results (inside points only)\n"
  text += PrintArray(paras[7], 4)

  text += "# Field derivative results (inside points only)\n"
  text += PrintArray(paras[8], 4)

  return text

#==============================================================================
numCellsOfAType = 8

sys.stdout.write("//==========================================================================\n")
sys.stdout.write("// This file was generated by \"" + sys.argv[0] + "\"\n")
sys.stdout.write("// Seed: " + str(seed) + "\n")
sys.stdout.write("//==========================================================================\n")
sys.stdout.write("\n")
sys.stdout.write("#ifndef lcl_testing_internal_TestDataCells_h\n")
sys.stdout.write("#define lcl_testing_internal_TestDataCells_h\n")
sys.stdout.write("\n")
sys.stdout.write("#include <cstddef>\n")
sys.stdout.write("\n")
sys.stdout.write("namespace lcl {\nnamespace testing {\nnamespace internal {\n")
sys.stdout.write("\n")
sys.stdout.write("constexpr size_t NumInsideTestPoints = " + str(numInsideTestPoints) + ";\n")
sys.stdout.write("constexpr size_t NumOutsideTestPoints = " + str(numOutsideTestPoints) + ";\n")
sys.stdout.write("constexpr size_t NumTestPoints = NumInsideTestPoints + NumOutsideTestPoints;\n")
sys.stdout.write("\n")
sys.stdout.write("const char* TestDataText = {\n")

generators = [GenerateRandomVertex,
              GenerateRandomLine,
              GenerateRandomTriangle, GenerateRandomPolygon, GenerateRandomQuad, GenerateRandomPixel,
              GenerateRandomTetra, GenerateRandomHex, GenerateRandomVoxel,
              GenerateRandomWedge, GenerateRandomPyramid]

ValidatorFailedCount = 0
WriteFailedCells = False

cellIdx = 0
for genCell in generators:
  for i in range(0, numCellsOfAType):
    cell = None

    success = False
    while not success:
      cell = genCell()

      validator = vtk.vtkCellValidator()
      status = validator.Check(cell, 1e-3)
      if status == 0:
        success = True
      else:
        sys.stderr.write("cell validator failed: " + str(cell.GetCellType()) + " {0:b}".format(status) +"\n")
        if WriteFailedCells:
          fileName = "failed-cell.{:>04d}.vtk".format(ValidatorFailedCount)
          ValidatorFailedCount += 1
          WriteCell(cell.GetCellType(), pts, fileName)

    paras = GenerateTestParas(cell)

    sys.stdout.write("\"#-[" + str(cellIdx) + "]" + "-"*70 + "\\n\"\n")
    cellIdx += 1

    text = PrintTestParas(paras)
    for line in text.splitlines():
      sys.stdout.write("\"" + line + "\\n\"\n")
    sys.stdout.write("\n")

sys.stdout.write("};\n")
sys.stdout.write("}}} // namespace lcl::testing::internal\n")
sys.stdout.write("\n")
sys.stdout.write("#endif // lcl_testing_internal_CellTestData_h\n")
