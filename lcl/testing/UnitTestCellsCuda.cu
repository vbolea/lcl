//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.md for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
#include "TestingCells.h"

#include <thrust/device_vector.h>

#include <functional>


namespace
{

template <typename Func, typename... Args>
__global__ void runTask(Func func, size_t count, Args... args)
{
  auto idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx < count)
  {
    func(idx, args...);
  }
}

class CudaExecutor
{
private:
  template <typename T>
  struct ArgIn
  {
    const std::vector<T>* HostVec;
    thrust::device_vector<T> DeviceVec;
  };

  template <typename T>
  struct ArgOut
  {
    std::vector<T>* HostVec;
    thrust::device_vector<T> DeviceVec;
  };

public:
  template <typename Functor, typename... Args>
  void run(const Functor& func, size_t count, Args... args) const
  {
    constexpr size_t threadsPerBlock = 256;
    auto nblocks = (count + threadsPerBlock - 1) / threadsPerBlock;

    preProcess(args...);
    runTask<<<nblocks, threadsPerBlock>>>(func, count, ToDevice<Args>::get(args)...);
    postProcess(args...);
  }

  template <typename T>
  static ArgIn<T> in(const std::vector<T>& arg)
  {
    ArgIn<T> ret;
    ret.HostVec = &arg;
    return ret;
  }

  template <typename T>
  static ArgOut<T> out(std::vector<T>& arg)
  {
    ArgOut<T> ret;
    ret.HostVec = &arg;
    return ret;
  }

private:
  template <typename T>
  struct ToDevice
  {
    static T& get(T& arg)
    {
      return arg;
    }
  };

  template <typename T>
  struct ToDevice<ArgIn<T>>
  {
    static const T* get(const ArgIn<T>& arg)
    {
      return arg.DeviceVec.data().get();
    }
  };

  template <typename T>
  struct ToDevice<ArgOut<T>>
  {
    static T* get(ArgOut<T>& arg)
    {
      return arg.DeviceVec.data().get();
    }
  };

  template <typename T>
  static void preProcess(ArgIn<T>& arg)
  {
    arg.DeviceVec.resize(arg.HostVec->size());
    thrust::copy(arg.HostVec->begin(), arg.HostVec->end(), arg.DeviceVec.begin());
  }

  template <typename T>
  static void preProcess(ArgOut<T>& arg)
  {
    arg.DeviceVec.resize(arg.HostVec->size());
  }

  template <typename T>
  static void preProcess(T&&)
  {
  }

  template <typename T1, typename... Ts>
  static void preProcess(T1&& arg1, Ts&&... args)
  {
    preProcess(std::forward<T1>(arg1));
    preProcess(std::forward<Ts>(args)...);
  }

  template <typename T>
  static void postProcess(ArgOut<T>& arg)
  {
    thrust::copy(arg.DeviceVec.begin(), arg.DeviceVec.end(), arg.HostVec->begin());
  }

  template <typename T>
  static void postProcess(T&&)
  {
  }

  template <typename T1, typename... Ts>
  static void postProcess(T1&& arg1, Ts&&... args)
  {
    postProcess(std::forward<T1>(arg1));
    postProcess(std::forward<Ts>(args)...);
  }
};

} // anonymous namespace

int main()
{
  lcl::testing::TestCells(CudaExecutor());
  return 0;
}
